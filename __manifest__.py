# -*- coding: utf-8 -*-
{
    'name': "project_timeline_overflow",

    'description': """
        Allow overflow of text in timeline
    """,

    'author': "Marco Marinello - continuity.space SRL",
    'website': "https://continuity.space",
    'license': 'AGPL-3',


    # Categories can be used to filter modules in modules listing
    # Check https://github.com/flectra/flectra/blob/master/flectra/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'project', 'project_timeline_hr_timesheet'],

    # always loaded
    'data': [
        'views/web_timeline_assets_backend.xml',
    ],
}
